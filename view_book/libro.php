<?php
	function __autoload($class)
	{
		require "../php/class." . $class . ".php";
	}
	
	$infoUser = User::verif_sesion();
	
	//echo $infoUser['username'];
	$isbn = $_GET["isbn"];
	$bookDetail = Book::selectBook($isbn);
	
	//echo $infoUser['username'];
	
	$mostrar = "";
	$signin=false;
	
	if(!isset ($infoUser['username'] )){
		$mostrar = "Iniciar Sesi&#243;n"; 
	}else{
		$signin=true;
		$mostrar = $infoUser['username'];
	}
		
		//header("Location: ../usuario_inicio");
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link type='text/css' rel='stylesheet' href='../css/b1.css' />
		<script language="JavaScript" type='text/javascript' src='../js/mootools-core-1.3.2.js'> </script>
		<script language="JavaScript" type='text/javascript' src='../js/mootools-more-1.3.2.1.js'></script>
		<script languaje='JavaScript' type='text/javascript' src='../js/load_libro.js'></script>
		<script languaje='JavaScript' type='text/javascript' src='../js/documento.js'></script>
		
		<script type="text/javascript">
				function buyBook(temp) {
					if(temp != "no"){
						$("principal").load("../php/dao.insert.sale.php?isbn=" + temp);
					}
					
				}

				function sesion() {
					window.location = "";
				}
					
				</script>
				
				<style type="text/css">
				
				/*Validacion Formulario*/

				form#user_reg [required]{
					/*border:solid 1px red;*/
					outline: 1px solid red;
				}
 
				/*input:required{
					outline: 1px solid red;
				}*/
 
 
				
				#izq{
					width:180px; 
					height:250px; 
					margin: 0 40px; 
					padding: 0 30px; 
					float:left;	
				}
				
				#der{
					width:180px; 
					padding: 0 30px 0 0; 
					float:left;	
				}
				
				.usr_reg{
					margin-bottom: 5px;
				}
				
				
				#envio{
					margin-top: 15px;
					margin-left: -150px;
				}
				
				
				
				</style>
				
        <title>Plantilla</title>
    </head>
    <body>
        <div id='wrapper'>
        	<header>
        		<nav id='sesion'>
					<ul class='botones'>
						<?php 
					if($signin){
//
					?>
						<li><a id="sesion_personal" href="user_ver.php" title="Informacion Personal"> <?php echo $mostrar;  ?> </a></li>
					<?php 
					}else{
					?>
						<li><a id="sesion_personal" href="../view_user/sesion.php" title="Informacion Personal"> <?php echo $mostrar;  ?> </a></li>
					<?php 
					}
					?>
					<?php
					if ($signin) {
						//
						?>
					<li><a id="sesion_salir" href="../php/sesion_cerrar.php"
						title="Cerrar Sesion">Salir</a></li>
					<?php
					}
					?>
					</ul>
				</nav>
        		<a href="../index.php" ><img src='../css/img/logo.png' alt='Logo Books'/></a>
				<div id='title'>
					<h1>Mi Mundo</h1>
				</div>
        	</header>
			
			<div id='advVert'>
				<a target="_blank" href="libro.php?isbn=9788445001769">
				<img width="160" height="600" border="0" src="../img/wideSkyscraper.png">
			</a>
			</div>
			
			<section >
			
			<div id='principal'>
			<h2> <?php echo $bookDetail[1] ?> </h2>
				
							
				<form name="user_reg" id="user_reg" method="post" action="../php/dao.insert.sale.php">
					<div id='izq'>
					
					
						<img width="width:100px;" height="200px;" src='<?php echo '../img/' . $bookDetail[0] . '.jpg';  ?>' alt='cover'/>
						<div class='usr_reg'>
							<label for="isbn"> isbn: </label>
							<label><?php echo $bookDetail[0] ?></label>
							<input type="hidden" id="isbn" name="isbn" size="15"  value="<?php echo $bookDetail[0] ?>" maxlength="20" required/>
							<input type="hidden" id="user" name="user" size="15"  value="<?php echo $infoUser['idusers']?>" maxlength="20" required/>
							<input type="hidden" id="stock" name="stock" size="15"  value="<?php echo $bookDetail[7] ?>" maxlength="20" required/>
						</div>
						<a id="volver" href="../index.php">Volver</a>
					</div>
					<div id='der'>
						<div class='usr_reg'>
							<label for="title"> titulo: </label>
							<label><?php echo strtoupper($bookDetail[1]) ?></label>
						</div>
						<div class='usr_reg'>
							<label for="author"> autor: </label>
							<label><?php echo strtoupper($bookDetail[2]) ?></label>
						</div>
						<div class='usr_reg'>
							<label for="editorial"> editorial: </label>
							<label><?php echo strtoupper($bookDetail[3]) ?></label>
						</div>
						<div class='usr_reg'>
							<label for="edition">edicion: </label>
							<label><?php echo $bookDetail[4] ?></label>
						</div>
						<div class='usr_reg'>
							<label for="cantidad"> cantidad: </label>
							<label><?php echo $bookDetail[7] ?></label>
						</div>
						
						<div class='usr_reg'>
							<label for="price"> precio: </label>
							<label>$ <?php echo $bookDetail[5] ?></label>
						</div>
						<br />
						<?php 
							if($signin){
								if($bookDetail[7] > 0 && $bookDetail[6] != 1  ){

						?>
							<button id='venta' onclick='buyBook(<?php echo "$isbn"; ?>)' >Comprar</button>
							<?php 
								}elseif ($bookDetail[7] < 1 ){
							?>
									Libro Agotado
							<?php 
								}else{
									echo "comprado";
								}
							}else{
							?>
							<a  id="volver" href="../view_user/sesion.php">Iniciar Sesi&#243;n</a>
							<?php 
							}
							?>
					</div>
				</form>
			</div>
			</section>
			
			<footer  id='sesion'>
				<div class='classFooter'>
					<a id="sesion_avisoPriva" href="javascript:void(0)" title="Aviso de Privacidad">Privacidad</a>
				</div>
				<div class='classFooter'>
				<a id="sesion_terminos" href="javascript:void(0)"
					title="Aviso de Privacidad">Terminos y Condiciones</a>
			</div>
			</footer>
        </div>
    </body>
</html>
