<?php
	function __autoload($class)
	{
		require "../php/class." . $class . ".php";
	}
	
	$infoUser = User::verif_sesion();
	
	//echo $infoUser['username'];
	
	$mostrar = "";
	$signin=false;
	
	if($infoUser['rol'] != 1 && $infoUser['rol'] != 2){
		header("Location: ../view_user/index.php");
	}else{
		$signin=true;
		$mostrar = $infoUser['username'];
	}
		
		//header("Location: ../usuario_inicio");
	
	if (! empty ( $_GET )) {
		$isbn = $_GET ['isbn'];
	}
	$detail = Book::selectBook($isbn);
	
		
		//header("Location: ../usuario_inicio");
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link type='text/css' rel='stylesheet' href='../css/b1.css' />
		<script language="JavaScript" type='text/javascript' src='../js/mootools-core-1.3.2.js'> </script>
		<script language="JavaScript" type='text/javascript' src='../js/mootools-more-1.3.2.1.js'></script>
		<script languaje='JavaScript' type='text/javascript' src='../js/load_libro.js'></script>
		<script languaje='JavaScript' type='text/javascript' src='../js/documento.js'></script>
		
		<script type="text/javascript">
				function buyBook(temp) {
					$("principal").load("../php/dao.insert.sale.php?isbn=" + temp);
				}
					
				</script>
				
				<style type="text/css">
				
				/*Validacion Formulario*/

				form#user_reg [required]{
					/*border:solid 1px red;*/
					outline: 1px solid red;
				}
 
				/*input:required{
					outline: 1px solid red;
				}*/
 
 
				
				#izq{
					width:180px; 
					height:250px; 
					margin: 0 40px; 
					padding: 0 30px; 
					float:left;	
				}
				
				#der{
					width:180px; 
					padding: 0 30px 0 0; 
					float:left;	
				}
				
				.usr_reg{
					margin-bottom: 5px;
				}
				
				
				#envio{
					margin-top: 15px;
					margin-left: -150px;
				}
				
				#principal{
					height: 350px;
				}
				
				</style>
				
        <title>Plantilla</title>
    </head>
    <body>
        <div id='wrapper'>
        	<header>
        		<nav id='sesion'>
					<ul class='botones'>
						<?php 
						if($signin){
//
					?>
						<li><a id="sesion_personal" href="javascript:void(0)" title="Informacion Personal"> <?php echo $mostrar;  ?> </a></li>
					<?php 
					}
					?>
						<li><a id="sesion_salir" href="javascript:void(0)" title="Cerrar Sesion">Salir</a></li>
					</ul>
					<ul class='botonesRoles'>
					
					<?php 
					if($infoUser[rol] == 1){
//
					?>
						<li><a id="sesion_admin" href="../view_user/admin.php" title="Adminstrar"> Administrar</a></li>
					<?php 
					}elseif ($infoUser[rol] == 2){
					?>
						<li><a id="sesion_admin" href="../view_user/admin.php" title="Adminstrar"> Administrar</a></li>
						<li><a id="sesion_owner" href="../view_user/propietario.php" title="Estadisticas"> Estadisticas</a></li>
					<?php 
					}
					?>
					</ul>
				</nav>
        		<a href="../index.php" ><img src='../css/img/logo.png' alt='Logo Books'/></a>
				<div id='title'>
					<h1>Mi Mundo</h1>
				</div>
        	</header>
			
			
			
			<section >
			
			<div id='principal'>
			<h2> <?php echo $bookDetail[1] ?> </h2>
				<form name="user_reg" id="user_reg" method="post" action="../php/dao.update.book.php">
					<div id='izq'>
						<div class='usr_reg'>
							<input type="hidden" id="isbn" name="isbn" size="15"  value="<?php echo $detail[0] ?>" maxlength="20" required/>
							<label for="isbn"> isbn: </label>
							<label><?php echo $detail[0]; ?></label>
						</div>
						<div class='usr_reg'>
							<label for="title"> titulo: </label>
							<input type="text" id="author" value="<?php echo $detail[1];  ?>" name="title" size="15" maxlength="20" required/>
						</div>
						<div class='usr_reg'>
							<label for="author"> autor: </label>
							<input type="text" id="author" value="<?php echo $detail[2];  ?>" name="author" size="15" maxlength="20" required/>
						</div>
						<div class='usr_reg'>
							<label for="editorial"> editorial: </label>
							<input type="text" id="editorial" value="<?php echo $detail[3];  ?>" name="editorial" size="15" maxlength="20" required/>
						</div>
						<div class='usr_reg'>
							<label for="edition">edici&#243;n: </label>
							<input type="text" id="edition" value="<?php echo $detail[4];  ?>" name="edition" size="15" maxlength="20" required/>
						</div>
						<div class='usr_reg'>
							<label for="cantidad"> cantidad: </label>
							<input type="text" id="cantidad" value="<?php echo $detail[7];  ?>" name="cantidad" size="15" maxlength="10" required/> 
						</div>
						<div class='usr_reg'>
							<label for="price"> precio: </label>
							<input type="text" id="price" value="<?php echo $detail[5];  ?>" name="price" size="15" maxlength="10" required/> 
						</div>
						<a id="volver" href="../view_user/admin.php">Volver</a>
					</div>
					<div id='der'>
					
						
						<br />
							<input name="reg_enviar" id="reg_enviar" type="submit" value="Actualizar"/>
					</div>
				</form>
			</div>
			</section>
			
			<footer  id='sesion'>
				<div class='classFooter'>
					<a id="sesion_avisoPriva" href="javascript:void(0)" title="Aviso de Privacidad">Privacidad</a>
				</div>
				
			</footer>
        </div>
    </body>
</html>
