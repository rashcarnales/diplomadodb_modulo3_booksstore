<?php
	function __autoload($class)
	{
		require "../php/class." . $class . ".php";
	}
	$infoUser = User::verif_sesion();
	
	if(!isset ($infoUser['username'] )){
		$mostrar = "Iniciar Sesi&#243;n";
	}else{
		$signin=true;
		$mostrar = $infoUser['username'];
	}
	
	if (! empty ( $_GET)) {
		$idusers = $_GET ['idusers'];
	}

	$detail = User::selectUser( $infoUser['idusers'] );
	
		
		//header("Location: ../usuario_inicio");
?>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type='text/css' rel='stylesheet' href='../css/b1.css' />
<script language="JavaScript" type='text/javascript'
	src='../js/mootools-core-1.3.2.js'> </script>
<script language="JavaScript" type='text/javascript'
	src='../js/mootools-more-1.3.2.1.js'></script>
<script languaje='JavaScript' type='text/javascript'
	src='../js/load_libro.js'></script>
<script languaje='JavaScript' type='text/javascript'
	src='../js/documento.js'></script>

<script type="text/javascript">
					
				</script>

<style type="text/css">

/*Validacion Formulario*/
form#user_reg [required] {
	/*border:solid 1px red;*/
	outline: 1px solid red;
}

/*input:required{
					outline: 1px solid red;
				}*/
#izq {
	width: 180px;
	height: 250px;
	margin: 0 40px;
	padding: 0 30px;
	float: left;
}

#der {
	width: 180px;
	padding: 0 30px 0 0;
	float: left;
}

.usr_reg {
	margin-bottom: 5px;
}

#envio {
	margin-top: 15px;
	margin-left: -150px;
}

#principal {
	height: 350px;
}
</style>

<title>Plantilla</title>
</head>
<body>
	<div id='wrapper'>
		<header>
			<nav id='sesion'>
					<ul class='botones'>
						<?php 
					if($signin){
//
					?>
						<li><a id="sesion_personal" href="javascript:void(0)" title="Informacion Personal"> <?php echo $mostrar;  ?> </a></li>
					<?php 
					}else{
					?>
						<li><a id="sesion_personal" href="index.php" title="Informacion Personal"> <?php echo $mostrar;  ?> </a></li>
					<?php 
					}
					?>
						<li><a id="sesion_salir" href="../php/sesion_cerrar.php" title="Cerrar Sesion">Salir</a></li>
					</ul>
				</nav>
			<a href="../index.php" ><img src='../css/img/logo.png' alt='Logo Books'/></a>
			<div id='title'>
				<h1>Mi Mundo</h1>
			</div>
		</header>



		<section>

			<div id='principal'>
				<h2> Editar Usuario </h2>


				<form name="user_reg" id="user_reg" method="post" action="../php/dao.update.user.php">
					<div id='izq'>
						<div class='usr_reg'>
						
							<input type="hidden" id="idusers" name="idusers" size="15"  value="<?php echo $detail[0] ?>" maxlength="20" required/>
							<label for="username"> username: </label>
							<input type="text" id="username" value="<?php echo $detail[1];  ?>" name="username" size="15" maxlength="20" required/>
						</div>
						<div class='usr_reg'>
							<label for="nom_pri"> nombre: </label>
							<input type="text" id="nom_pri" value="<?php echo $detail[2];  ?>" name="name" size="15" maxlength="10" required/> 
						</div>
						<div class='usr_reg'>
							<label for="ap_pat"> apellido paterno: </label>
							<input type="text" id="ap_pat" value="<?php echo $detail[3];  ?>" name="surname" size="15" maxlength="10" required/>
						</div>
						
						<div class='usr_reg'>
							<label for="rol"> Rol: </label>
							<?php 
if($detail[4] == 0){
?>

<input type="hidden" id="rol" name="rol" size="15"  value="<?php echo $detail[4] ?>" maxlength="20" required/>
<?php 
}else{
						
echo "<select name='rol' id='list$idusers'>";
							
for ($i = 0; $i < 4; $i++) {
	echo "<option value='$i'";
	if($i == $detail[4]){
		echo "selected";
	}
	echo ">";
											
	if($i == 0){
		echo "Normal";
	}else if($i == 1){
		echo "Administrador";
	}else if($i ==2){
		echo "Propietario";
	}else if($i ==3){
		echo "Bloquear";
	}
	echo "</option>";
}
echo "</select>";		
}								?>
						</div>
						
						<input name="reg_enviar" id="reg_enviar" type="submit" value="Actualizar"/>
						<a id="volver" href="../index.php">Volver</a>
					</div>
					<div id='der'>
						<div class='usr_reg'>
							Introduce la contrase&#241;a: si quieres modificarla
							<br />
							<label for="pass1"> contrase&#241;a: </label>
							<input type="password" id="pass1" name="pass1" size="15" maxlength="10" />
						</div>
						<div class='usr_reg'>
							<label for="pass2">repite la contrase&#241;a: </label>
							<input type="password" id="pass2" name="pass2" size="15" maxlength="10" />
						</div>
						<br />
							
					</div>
				</form>
			</div>
		</section>

		<footer  id='sesion'>
				<div class='classFooter'>
					<a id="sesion_avisoPriva" href="javascript:void(0)" title="Aviso de Privacidad">Privacidad</a>
				</div>
				
			</footer>
	</div>
</body>
</html>
