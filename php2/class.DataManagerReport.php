<?php
	
	class DataManagerReport{
		
		
		
		private $strDSN = "pgsql:dbname=phirmadigital;host=localhost;port=5432;user=phirmadigital_report;password=unam";
		private $hDB = "";
		
		
		private static function __getConnection(){
			try{
				$this->hDB = new PDO($this->strDSN);
			} catch (PDOException $e){
				echo "An error occurred connecting: " . $e->getMessage() . "\n";
				exit (0);
			}
		}
		
		public static function getCatalogoData($entidad){
			$entidad = strtoupper($entidad);
			$strQuery = "SELECT * FROM \"$entidad\"";
			$objStatement  = $this->hDB->prepare($strQuery);
			$objStatement->execute();
			return $objStatement;
		}
			
	}

?>