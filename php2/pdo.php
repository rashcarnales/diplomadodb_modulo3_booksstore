<?php
	$strDSN = "pgsql:dbname=phirmadigital;host=localhost;port=5432;" .
			"user=phirmadigital_report;password=unam";
			
	try{
		$objPDO = new PDO($strDSN);
		print " Successfully connected ...\n";
	} catch (PDOException $e){
		echo "An error occurred connecting: " . $e->getMessage() . "\n";
		exit (0);
	}
	
	$i=0;
	$strQuery = "SELECT * FROM \"CATALOG_AREA\"";
	$ObjStatement = $objPDO->query($strQuery);
	foreach($ObjStatement as $arRow){
		print "Row $i <br/> \n";
		foreach($arRow as $key => $value){
			if(!is_numeric($key)) print"- Column $key, value $value <br/> \n";
		}
		$i++;
	}
	
	$objPDO = NULL;
	print "Successfully disconnected.\n";
?>