<?php
	require_once('interface.Validator.php');
	
	abstract class PropertyObject implements Validator{
		
		protected $propertyTable = array();
		
		protected $changedProperties = array();
		
		protected $data;
		
		protected $errors = array();
		
		public function __construct($arData){
			$this->data = $arData;
		}
		
		public function __get($propertyName){
			if(!array_key_exists($propertyName, $this->propertyTable)){
				throw new Exception("Propiedad Invalida \"$propertyName\"!");
			}
			if(method_exists($this, 'get' . $propertyName)){
				return call_user_func(array($this, 'get' . $propertyName));
			} else{
				return $this->data[$this->propertyTable[$propertyName]];
			}
		}
		
		public function __set($propertyName, $value){
			if(!array_key_exists($propertyName, $this->propertyTable)){
				throw new Exception("Propiedad Invalida \"$propertyName\"!");
			}
			if(method_exists($this, 'set' . $propertyName)){
				return call_user_func(array($this, 'set' . $propertyName));
			} else{
				if($this->propertyTable[$propertyName] != $value && !in_array($propertyName, $this->changedProperties)){
					$this->changedProperties[] = $propertyName;
				}
				$this->data[$this->propertyTable[$propertyName]] = $value;
			}
		}
		
		public function validate(){
			
		}
	}
	
?>