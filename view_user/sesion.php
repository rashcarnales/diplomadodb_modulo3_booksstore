<?php

function __autoload($class)
{
	require "../php/class." . $class . ".php";
}

$infoUser = User::verif_sesion();

//echo $infoUser['username'];


if(!isset ($infoUser['username'] )){
	
}else{
	header("Location: ../index.php");
}
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link type='text/css' rel='stylesheet' href='../css/a1.css' />
		<title>inicio</title>
    </head>
    <body>
        <div id='wrapper'>
        	<header>
        		<a href="index.php" ><img src='../css/img/logo.png' alt='Logo Books'/></a>
				<div id='title'>
					<h1>Mi Mundo</h1>
				</div>
        	</header>
			
			<section id='principal'>
				<h2> Inicio sesi&#243;n </h2>
				<form id="sesion_form" name='sesion_form' method="post" action="../php/sesion.start.php">
					<div class='inicio'>
						<label for="id"> Username: </label>
						<input type="text" id="id" name="id" placeholder="a@a.com" title="Correo electronico" required autofocus/>
					</div>
					
					<div class='inicio'>
						<label for="pass"> Contrase&#241;a: </label>
						<input type="password" id="pass" name="pass" maxlength="10" title="Inserte su contrase&#241;a" required/>
					</div>
	
					<input id='enviar' type="submit" value="Enviar">
				</form>
				
				<a id='nuevo_trabajador' href="user_register.php">registro</a>
			</section>
			
			
        </div>
    </body>
</html>