<?php
	function __autoload($class)
	{
		require "../php/class." . $class . ".php";
	}
	
	$infoUser = User::verif_sesion();
	
	//echo $infoUser['username'];
	
	$mostrar = "";
	$signin=false;
	
	if($infoUser['rol'] != 1 && $infoUser['rol'] != 2){
		header("Location: index.php");
	}else{
		$signin=true;
		$mostrar = $infoUser['username'];
	}
		
		//header("Location: ../usuario_inicio");
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link type='text/css' rel='stylesheet' href='../css/b1.css' />
		<script language="JavaScript" type='text/javascript' src='../js/mootools-core-1.3.2.js'> </script>
		<script language="JavaScript" type='text/javascript' src='../js/mootools-more-1.3.2.1.js'></script>
		<script languaje='JavaScript' type='text/javascript' src='../js/load_admin.js'></script>
		<script languaje='JavaScript' type='text/javascript' src='../js/documento.js'></script>
        <title>Plantilla</title>
    </head>
    <body>
        <div id='wrapper'>
        	<header>
        		<nav id='sesion'>
					<ul class='botones'>
					<?php 
						if($signin){
//
					?>
						<li><a id="sesion_personal" href="../view_book/user_ver.php" title="Informacion Personal"> <?php echo $mostrar;  ?> </a></li>
					<?php 
					}
					?>
						<li><a id="sesion_salir" href="javascript:void(0)" title="Cerrar Sesion">Salir</a></li>
					</ul>
					<ul class='botonesRoles'>
					
					<?php 
					if($infoUser[rol] == 1){
//
					?>
						
					<?php 
					}elseif ($infoUser[rol] == 2){
					?>
						
						<li><a id="sesion_owner" href="propietario.php" title="Estadisticas"> Estadisticas</a></li>
					<?php 
					}
					?>
					</ul>
				</nav>
        		<a href="../index.php" ><img src='../css/img/logo.png' alt='Logo Books'/></a>
				<div id='title'>
					<h1>Mi Mundo</h1>
				</div>
        	</header>
			
			<nav id='menu'>
				<h2>Administrar</h2>
				<ul class='botones'>
					<li><a id="users_table" href="javascript:void(0)" title="Todos los libros">Usuarios</a></li>
					<li><a id="books_table" href="javascript:void(0)" title="">Libros</a></li>
				</ul>
			</nav>
			
			<section id='principal'>
				<h2>Seccion</h2>
				<p id="contenido">
					<br />
					<br />
					
					
				</p>
			</section>
			
			<footer  id='sesion'>
				<div class='classFooter'>
					<a id="sesion_avisoPriva" href="javascript:void(0)" title="Aviso de Privacidad">Privacidad</a>
				</div>
				<div class='classFooter'>
					<a id="sesion_terminos" href="javascript:void(0)" title="Terminos y Condiciones">Terminos y Condiciones</a>
				</div>
				
			</footer>
        </div>
    </body>
</html>
