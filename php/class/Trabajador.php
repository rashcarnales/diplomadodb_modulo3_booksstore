<?php
	
	class Trabajador extends Dao
	{
		private $dato = false;
		
		function __construct($user="phirmadigital_report", $type){
			parent::__construct($user);
			
			
			
			if ($type == "POST")
			{
				
				if(!empty($_POST))
				{
					$this->dato = $_POST;
				}
				else
					echo "POST no cargado";
			}	
			elseif ($type == "GET")
			{
				if(!empty($_GET))
					$dato = clone $_GET;
				else
					echo "GET no cargado";
			}		
			else
				"pendiente session";
			
		}
		
		public function access()
		{
			
		}
		
		public function verifDatos()
		{
			if (!empty($this->dato))
			{
				foreach (($this->dato) as $key => $value)
				{
					echo $key, " => ", $value, " <br />";
				}
			}
		}
		
		
		function compararPass()//compara pass y pass2
		{
			
			if(($this->dato['pass']) == ($this->dato['pass2']))
				return true;
			else
				return false;
			
		}
		
		function modificPass()//codifica el pass a md5 y borra pass2
		{
			$this->dato['pass'] = md5($this->dato['pass']);
			//echo $this->dato['pass'];
			unset($this->dato['pass2']);
		}
		
		function dbInsert()
		{
			$numeros = array(true, false, false, false, false, false, false, true, true);
			$contador = 0;
			
			$atributo = "id, pass, nombre_primero, nombre_segundo, apellido_paterno, apellido_materno," .
					"telefono, catalog_area_id, catalog_puesto_id";
			$valor = "";
			
				foreach (($this->dato) as $key => $value)
				{
					if($numeros[$contador])
					{
						$valor .= $value . ", ";
					}
					else
						$valor .= "'" . $value . "', ";
						
					$contador++; 
				}
			$valor = substr ($valor, 0, strlen($valor) - 2);
			$sql = "INSERT INTO \"TRABAJADOR\" ($atributo) VALUES ($valor)";
			
			$query = pg_query($this->conexion, $sql)
				or die ("La consulta fallo: ".pg_error());
		}
		
	}

?>