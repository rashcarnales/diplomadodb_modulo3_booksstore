<?php
	class Registro{
		
		private static function detectar_ip(){
	
			//Conseguir la Direccion ip
			if (getenv("HTTP_X_FORWARDED_FOR")) {//direccion del proxy si existe
     			$ip['proxy'] = getenv("HTTP_X_FORWARDED_FOR");
   			} else {
    	 	//IP valida de la computadora
     			$ip['remota']   = getenv("REMOTE_ADDR");
   			} 
   			
   			return 	$ip;	
		}
		
		private static function detectar_navegador(){
			
			if(strrpos($_SERVER["HTTP_USER_AGENT"], "MSIE"))
   				$navegador = "Internet Explorer";
   			elseif(strrpos($_SERVER["HTTP_USER_AGENT"], "Firefox"))
   				$navegador = "Firefox";
   			elseif(strrpos($_SERVER["HTTP_USER_AGENT"], "Chrome"))
   				$navegador = "Chrome";
   			elseif(strrpos($_SERVER["HTTP_USER_AGENT"], "Opera"))
   				$navegador = "Opera";
   			elseif(strrpos($_SERVER["HTTP_USER_AGENT"], "Safari"))
   				$navegador = "Safari";
   			elseif(strrpos($_SERVER["HTTP_USER_AGENT"], "Konqueror"))
   				$navegador = "Konqueror";
   			else
   				$navegador = "otro";
   				
   			return $navegador;
		}
		
		
		public static function insert($trabajador_id){
			
   			$ip = self::detectar_ip();
   			$ip_proxy = $ip['proxy'];
   			$ip_remota = $ip['remota'];
   			$fecha = Fecha::now();
   			$navegador = self::detectar_navegador();
   			
   			$datos = array("sip" => "$ip_proxy", "sip_proxy" => $ip_remota, "trabajador_id" => $trabajador_id, 
			"tfecha" => "$fecha", "snavegador" => "$navegador");
   
   			
   			foreach ($datos as $index => $valor){
   				echo "$index tiene $valor" . "<br/>";
   			}
   			
   			DaoReport::insert("Registro", $datos);
		}
		
	}
?>