<?php
class Book {
	
	// private static $sesion;
	
	/*
	 * public static function getIdSesion(){ session_start(); $id = $_SESSION['id']; session_write_close(); return $id; } public static function iniciar_sesion($id){ $info = DaoReport::buscarTrabajador($id); if($info){ foreach($info as $dato => $valor){ echo "$dato tiene $valor" . "<br/>"; } } if ( md5($_POST["pass"]) == $info['spass']){ session_start(); $_SESSION['id']= "$id"; $_SESSION['nombre_primero']= $info['snombre_primero']; $_SESSION['apellido_paterno']= $info['sape_pat']; $id= (int)$id; Registro::insert($id); //include ("sesion_registro.php");//datos de sesion, ip, mac, etc header("Location: ../usuario_principal"); } else { echo "contraseña rechazada"; //header("Refresh: 3; URL=../"); } } public static function verif_sesion(){ session_start(); //echo $_SESSION['id']; if(isset($_SESSION['id'])){ session_write_close(); return true; } else{ session_write_close(); return false; } } private static function verif_captcha(){ session_start(); //echo $_SESSION['id']; if(isset($_SESSION['cadena']) && isset($_POST['captcha'])){ //$post="hola"; $post = strtolower($_POST['captcha']); $sesion = $_SESSION['cadena']; session_write_close(); echo "este es post :" . $post . " y este es sesion :" . $sesion; if($sesion == $_POST['captcha']){ return true; } else return false; } } public static function cerrar_sesion(){ session_start(); $_SESSION = array(); session_destroy(); echo "Redireccionando"; header("Refresh: 3; URL=../usuario_inicio"); }
	 */
	public static function insert() {
		// $enteros = array(true, false, false, false, false, false, false, true, true);
		
		// $atributo = "id, spass, snombre_primero, snombre_segundo, sapellido_paterno, sapellido_materno," .
		// "stelefono, catalog_area_id, catalog_puesto_id";
		if (! empty ( $_POST )) {
			$isbn = $_POST ['isbn'];
			$title = $_POST ['title'];
			$author = $_POST ['author'];
			$editorial = $_POST ['editorial'];
			$edition = $_POST ['edition'];
			$price = (int) $_POST ['price'];
		}
		$datos = array (
				"isbn" => trim($isbn),
				"title" => ucwords(strtolower($title)),
				"author" => ucwords(strtolower($author)),
				"editorial" => ucwords(strtolower($editorial)),
				"edition" => $edition,
				"price" => $price 
		);
		
		DaoReport::insert ( "books", $datos );
		header("Location: ../view_user/admin.php");
		echo "Libro insertado <br/> redireccionando";
	}
	
	
	public static function update() {
		// $enteros = array(true, false, false, false, false, false, false, true, true);
	
		// $atributo = "id, spass, snombre_primero, snombre_segundo, sapellido_paterno, sapellido_materno," .
		// "stelefono, catalog_area_id, catalog_puesto_id";
		if (! empty ( $_POST )) {
			$isbn = $_POST ['isbn'];
			$title = $_POST ['title'];
			$author = $_POST ['author'];
			$editorial = $_POST ['editorial'];
			$edition = $_POST ['edition'];
			$cantidad = (int) $_POST ['cantidad'];
			$price = (int) $_POST ['price'];
		}
		$datos = array (
				"id" => $isbn,
				"title" => $title,
				"author" => $author,
				"editorial" => $editorial,
				"edition" => $edition,
				"stock" => $cantidad,
				"price" => $price
		);
	
		DaoReport::update("books", $datos, "isbn");
		header("Location: ../view_user/admin.php");
		echo "libro actualizado <br/> redireccionando";
	}
	
	public static function delete() {
		// $enteros = array(true, false, false, false, false, false, false, true, true);
	
		// $atributo = "id, spass, snombre_primero, snombre_segundo, sapellido_paterno, sapellido_materno," .
		// "stelefono, catalog_area_id, catalog_puesto_id";
		if (! empty ( $_POST )) {
			$isbn = $_POST ['isbn'];
		}
		$datos = array (
				"id" => $isbn,
				"invisible" => 1,
		);
	
		DaoReport::update("books", $datos, "isbn");
	}
	
	public static function select(){
		DaoReport::getCatalogo();
	}
	
	public static function selectBook($isbn){
		
		session_start();
		$idUser = 0;
		$idUser= $_SESSION['idusers'];
		if($idUser < 1){
			$idUser = 0;
		}
		//echo "prueba" . $idUser;
		return DaoReport::getBook($isbn, $idUser);
	}
	
	public static function allBooks(){
		$books = DaoReport::getAllBook();
		
		
		return $books;
	}
	public static function allBooksMas(){
		$books = DaoReport::getAllBookMas();
	
	
		return $books;
	}
	public static function allBooksMenos(){
		$books = DaoReport::getAllBookMenos();
	
	
		return $books;
	}
	
	public static function myBooks($iduser){
		$books = DaoReport::getMyBook($iduser);
	
	
		return $books;
	}
}

?>