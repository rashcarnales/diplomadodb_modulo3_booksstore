<?php
	
	$image_height = 100;
	$image_width = 300;

	$image = imagecreate($image_width, $image_height);
	$back_color = imagecolorallocate($image, 200, 200, 200);
	$draw_color = imagecolorallocate($image, 0,0,0);
	imageline($image, 0, 0, 300, 100, $draw_color);
	header('Content-Type: image/jpeg');
	imagejpeg($image);
	imagedestroy($image);
?>