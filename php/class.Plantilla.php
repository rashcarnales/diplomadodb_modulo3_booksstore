<?php
	class Plantilla {
		
		private static function post(){
			
		}
		
		
		
		public static function insert(){
			if(isset($_POST['contenido']) && isset($_POST['nombre'])){
				
				
				$nombre = $_POST['nombre'];
				$contenido = $_POST['contenido'];
				$fecha = Fecha::now();
				$contenido = preg_split('/ /', $contenido, -1, PREG_SPLIT_OFFSET_CAPTURE);
				
				$id_trabajador = Trabajador::getIdSesion();
				$id_trabajador = (int)$id_trabajador;
				
				$tipo = "privada";
				
				$rut_img_max = "false";
				$rut_img_min = "false";
				
				$activo = "true";
				
				$datos = array("snombre" => "$nombre", "tfecha" => "$fecha", 
				"acontenido" => $contenido, "trabajador_id"  => $id_trabajador, "etipo" => "$tipo", 
				"srut_img_max" => "$rut_img_max", "srut_img_min" => $rut_img_min, "bactivo" => $activo);
				
				
				DaoReport::insert("Plantilla", $datos);
				
				self::setImg();
				
			}else{
				echo "datos invalidos";
			}
		}
		
		public static function setImg(){
			//imagen
			$width = 2550/3;
			$height = 3300/3;
			$image = imagecreate($width, $height);
			$back_color = imagecolorallocate($image, 250, 250, 250);
			
			
			
			//Datos Documento
			$margen_x = 70;
			$margen_y = 70;
			$doc['institucion'] =  "Universidad Nacional Autonoma de México";
			$doc['subinstitucion'] = "Facultad de Estudios Superiores Acatlan";
			$doc['division'] = "n/a";
			$doc['oficio'] = "n/a";
			
			
			$drawing_color = imagecolorallocate($image, 0, 0, 0);
			$font_number = 15;
			$fuente = "font/Amery.ttf";
			$interlineado = 20;
			$inicio_x = 0;
			$inicio_y = 0;
			//for($i= 0; $i< count($doc); $i++){
			$i = 0;
			foreach($doc as $index => $valor){	
				
				$bbox = imagettfbbox($font_number, 0, $fuente,$valor);
				$x_position = $width - $bbox[4]- $margen_x;
				
				//$x_position = $margen_x + $inicio_x;
				$y_position = $margen_y + $interlineado + $inicio_y + (23*$i);
				imagettftext($image,$font_number,0,$x_position,$y_position,$drawing_color,$fuente,$valor);
				$i++;
			}
			
			
			
			//DatosEmisor
			$emi['nombre'] = "n/a";
			$emi['cargo'] = "n/a";
			
			
			//Argumento
			$drawing_color = imagecolorallocate($image, 0, 0, 0);
			$font_number = 12;
			
			
			$origen = imagecreatefromgif('img/escudo_UNAM.gif');
			imagecopy($image, $origen, $margen_x, $margen_y, 0, 0, 160, 187);
			
			//380,444
			//obtiene el parrafo
			
			$id = 2;
			$trabajador_id = 1;
			$parrafo = self::parrafo();
			
			
			$interlineado = 20;
			$inicio = 500;
			for($i= 0; $i< count($parrafo); $i++){
				$x_position = $margen_x;
				$y_position = $margen_y + $interlineado + $inicio + ($i*20);
				imagettftext($image,$font_number,0,$x_position,$y_position,$drawing_color,"font/Amery.ttf",$parrafo[$i]);
			}
			
			//imagettftext($image,$font_number,0,$x_position,$y_position,$drawing_color,"../php/font/Amery.ttf",$text);
			session_start();
			if(isset($_SESSION['id_tabla']))
			{
				$id_tabla = $_SESSION['id_tabla'];
				$trabajador_id = $_SESSION['id'];
			}
			session_write_close();
			
			//Envio al navegador
			$ruta = "plantillas/normal/";
			$nombre = $ruta . self::nombre($trabajador_id, $id_tabla) . ".gif";
			header('Content-Type: image/gif');
			imagegif($image, $nombre);
			imagedestroy($image);
		}
		
		public static function nombre($trabajador_id, $id){
			$trabajador_id = (string)$trabajador_id;
			$id = (string)$id;
			
			
			$ceros = "";
			for ($i=0; $i<(5-strlen($trabajador_id)); $i++){
				$ceros .= "0";
			}
			$trabajador_id = $ceros . $trabajador_id;
			
			$ceros = "";
			for ($i=0; $i<(5-strlen($id)); $i++){
				$ceros .= "0";
			}
			$id = $ceros . $id;
			
			$nombre = $trabajador_id . $id . Fecha::dmy();
			
			return $nombre;
		}
		
		public static function parrafo(){
			
			
			$tabla = false;
			$tabla = DaoReport::getArreglo();
			
			if($tabla){
				
				//$tupla_id =$tabla[0];
				//$subelemento = $tabla;
				
				
				/*$tupla = false;
				foreach($tupla_id as $index => $valor){
					//echo "valor $valor , id: $id " . "<br/>";
					if($valor == $id){
						$tupla = $subelemento[$index];
					}
				}*/
				
				//Crea los renglones para el parrafo 
				$renglon = false;
				$limite = 50;
				
				//array_count_values();
				//if($tupla){
					
					$parrafo = "";
					foreach($tabla as $valor)
						$parrafo .= $valor[0] . " ";
					
					$parrafo2 = wordwrap($parrafo, $limite, "+");
					
				//}
				//echo $parrafo2;
				
				$parrafo3 = preg_split('/\+/', $parrafo2, -1, PREG_SPLIT_NO_EMPTY);
	
				/*foreach($parrafo3 as $index1 => $valor1){
					echo "$valor1" . "<br/>";
				}*/		
				
				return $parrafo3;	 
				/*foreach($subelemento0 as $index1 => $valor1){
					echo "index 1: $index1" . "<br/>";
					foreach($valor1 as $index2 => $valor2){
						echo "		index 2: $index2" . "<br/>";
						foreach($valor2 as $index3 => $valor3){
							echo "			Valor : $valor3" .  "<br/>"  . "<br/>";
						}				
					}
				}*/
			}
			
		}
		
		
		private static function insertarPalabra($oracion, $palabra, $posicion){
			return substr($oracion, 0, $posicion) . $palabra . substr($oracion, $posicion);
		}
	}
?>