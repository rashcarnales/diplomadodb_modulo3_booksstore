<?php
	class Fecha{
		public static function now(){
			date_default_timezone_set('America/Mexico_City');
			$fecha = date('Y-m-d H:i:s');
			//echo "$fecha";
			return $fecha;	
		}
		
		public static function dmy(){
			date_default_timezone_set('America/Mexico_City');
			$fecha = date('dmY');
			
			return $fecha;
		}
	}
?>