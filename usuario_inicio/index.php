<?php
	function __autoload($class)
	{
		require "../php/class." . $class . ".php";
	}
	
	if(Trabajador::verif_sesion())
		header("Location: ../usuario_principal");
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link type='text/css' rel='stylesheet' href='../css/a1.css' />
		<title>inicio</title>
    </head>
    <body>
        <div id='wrapper'>
        	<header>
        		<img src='../css/img/logo.png' alt='logo firma digital'/>
				<div id='title'>
					<h1>encabezado</h1>
				</div>
        	</header>
			
			<section id='principal'>
				<h2> INICIO SESION </h2>
				<form id="sesion_form" name='sesion_form' method="post" action="../php/sesion.iniciar.php">
					<div class='inicio'>
						<label for="id"> No Trabajador: </label>
						<input type="text" id="id" name="id" placeholder="12345" title="El numero de trabajador consta de 5 cifras" required autofocus/>
					</div>
					
					<div class='inicio'>
						<label for="pass"> Contrase�a: </label>
						<input type="password" id="pass" name="pass" maxlength="10" title="Inserte su contraseña" required/>
					</div>
	
					<input id='enviar' type="submit" value="Enviar">
				</form>
				
				<a id='nuevo_trabajador' href="../php/usuario_registro.php">Nuevo trabajador</a>
			</section>
			
			<footer>
				<p>pagina creada por Daniel Gerardo Varela Martinez</p>
			</footer>
        </div>
    </body>
</html>