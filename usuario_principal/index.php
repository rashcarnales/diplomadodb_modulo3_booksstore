<?php
	function __autoload($class)
	{
		require "../php/class." . $class . ".php";
	}
	
	if(!Trabajador::verif_sesion())
		header("Location: ../usuario_inicio");
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link type='text/css' rel='stylesheet' href='../css/b1.css' />
		<script language="JavaScript" type='text/javascript' src='../js/mootools-core-1.3.2.js'> </script>
		<script language="JavaScript" type='text/javascript' src='../js/mootools-more-1.3.2.1.js'></script>
		<script languaje='JavaScript' type='text/javascript' src='../js/load_principal.js'></script>
		<script languaje='JavaScript' type='text/javascript' src='../js/documento.js'></script>
        <title>Plantilla</title>
    </head>
    <body>
        <div id='wrapper'>
        	<header>
        		<nav id='sesion'>
					<ul class='botones'>
						<li><a id="sesion_personal" href="javascript:void(0)" title="Informacion Personal">configuracion</a></li>
						<li><a id="sesion_contacto" href="javascript:void(0)" title="Dudas y Sugerencias">Contacto</a></li>
						<li><a id="sesion_faq" href="javascript:void(0)" title="Preguntas">FAQ</a></li>
						<li><a id="sesion_salir" href="../usuario_final" title="Cerrar Sesion">Salir</a></li>
					</ul>
				</nav>
        		<img src='../css/img/logo.png' alt='logo firma digital'/>
				<div id='title'>
					<h1>Firma Digital</h1>
				</div>
        	</header>
			
			<nav id='menu'>
				<h2>Navegacion</h2>
				<ul class='botones'>
					<li><a id="menu_nuevo" href="javascript:void(0)" title="Escribir nuevo Documento">Nuevo</a></li>
					<li><a id="menu_recibido" href="javascript:void(0)" title="Documentos Recibidos">Entrada</a></li>
					<li><a id="menu_enviado" href="javascript:void(0)" title="Documentos Enviados">Salida</a></li>
					<li><a id="menu_eliminado" href="javascript:void(0)" title="Papeleria de Reciclaje">Eliminados</a></li>
				</ul>
			</nav>
			
			<section id='principal'>
				<h2>Seccion</h2>
				<p id="contenido">
					Las partes que tiene ajax son los botones de arriba: Usuario, Contacto y Salir.<br />
					Me fije mas en la presentacion, tratando de usar la mayor cantidad de propiedades,<br />
					 a la vez que llama a las funciones desde href en la etiqueta a.
					
				</p>
			</section>
			
			<footer>
				<p>pagina creada por Daniel Gerardo Varela Martinez</p>
			</footer>
        </div>
    </body>
</html>
