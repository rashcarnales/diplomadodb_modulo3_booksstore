<?php
function __autoload($class) {
	require "php/class." . $class . ".php";
}

$infoUser = User::verif_sesion ();

// echo $infoUser['username'];

$mostrar = "";
$signin = false;

if (! isset ( $infoUser ['username'] )) {
	$mostrar = "Iniciar Sesi&#243;n";
} else {
	$signin = true;
	$mostrar = $infoUser ['username'];
}

// header("Location: usuario_inicio");
?>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type='text/css' rel='stylesheet' href='css/catalogo.css' />
<script language="JavaScript" type='text/javascript'
	src='js/mootools-core-1.3.2.js'> </script>
<script language="JavaScript" type='text/javascript'
	src='js/mootools-more-1.3.2.1.js'></script>
<script languaje='JavaScript' type='text/javascript'
	src='js/load_principal.js'></script>
<script languaje='JavaScript' type='text/javascript'
	src='js/documento.js'></script>
<title>Mi Mundo</title>
</head>
<body>
	<div id='wrapper'>
		<header>
			<nav id='sesion'>
				<ul class='botones'>
					
					<?php
					if ($signin) {
						//
						?>
						<li><a id="sesion_personal" href="view_book/user_ver.php"
						title="Informacion Personal"> <?php echo $mostrar;  ?> </a></li>
					<?php
					} else {
						?>
						<li><a id="sesion_personal" href="view_user/sesion.php"
						title="Informacion Personal"> <?php echo $mostrar;  ?> </a></li>
					<?php
					}
					?>
						
						<li><a id="sesion_contacto" href="javascript:void(0)"
						title="Dudas y Sugerencias">Contacto</a></li>
					<?php
					if ($signin) {
						//
						?>
					<li><a id="sesion_salir" href="php/sesion_cerrar.php"
						title="Cerrar Sesion">Salir</a></li>
					<?php
					}
					?>
				</ul>

				<ul class='botonesRoles'>
					
					<?php
					if ($infoUser [rol] == 1) {
						//
						?>
						<li><a id="sesion_admin" href="view_user/admin.php" title="Adminstrar">
							Administrar</a></li>
					<?php
					} elseif ($infoUser [rol] == 2) {
						?>
						<li><a id="sesion_admin" href="view_user/admin.php" title="Adminstrar">
							Administrar</a></li>
					<li><a id="sesion_owner" href="view_user/propietario.php"
						title="Estadisticas"> Estadisticas</a></li>
					<?php
					}
					?>
					</ul>
			</nav>
			<a href="index.php"><img src='css/img/logo.png' alt='Logo Books' /></a>

			<div id='title'>
				<h1>Mi Mundo</h1>
			</div>
		</header>
		<div id="adv1">
			<a href="http://www.unam.mx/"><img id="mediumRectangle" width="180" height="150"
				alt="mediumRectangle" src="img/mediumRectangle.png"></a> <a href="http://www.unam.mx/"><img
				id="wideSkyscraper" width="160" height="600" alt="wideSkyscraper"
				src="img/wideSkyscraper.png"></a>
		</div>

		<div id="general">

			<nav id='menu'>
				<ul class='botones'>
					<li><a id="libros" href="javascript:void(0)"
						title="Todos los libros">Catalogo</a></li>
					<?php
					if ($signin) {
					?>
					<li><a id="misLibros" href="javascript:void(0)" title="Mis libros">Mis
							Compras</a></li>
					<?php
					}
					?>
				</ul>
			</nav>
			<section>
				<div id='principal'></div>
			</section>

		</div>

		<div id="adv2">
			<a href="http://www.unam.mx/"><img id="mediumRectangle" width="180" height="150"
				alt="mediumRectangle" src="img/mediumRectangle.png"></a> <a href="http://www.unam.mx/"><img
				id="wideSkyscraper" width="160" height="600" alt="wideSkyscraper"
				src="img/wideSkyscraper.png"></a>
		</div>

		<footer id='sesion'>
			<div class='classFooter'>
				<a id="sesion_avisoPriva" href="javascript:void(0)"
					title="Aviso de Privacidad">Privacidad</a>
			</div>
			<div class='classFooter'>
				<a id="sesion_terminos" href="javascript:void(0)"
					title="Aviso de Privacidad">Terminos y Condiciones</a>
			</div>

		</footer>
	</div>
</body>
</html>
